
Configuration for `Helix` editor and `Wezterm` terminal emulator
(based on their great documentation and community).

Put "helix" and "wezterm" folders in your "~/.config", and "hx_wezterm.sh" file on your path.
