#!/bin/sh

set -x

status_line=$(wezterm cli get-text | rg -e "(?:NOR|INS|SEL)\s+[\x{2800}-\x{28FF}]*\s+(\S*)\s[^│]* (\d+):*.*" -o --replace '$1 $2')
filename=$(echo "$status_line" | awk '{ print $1}')
line_number=$(echo "$status_line" | awk '{ print $2}')

split_pane_down() {
    bottom_pane_id=$(wezterm cli get-pane-direction down)
    if [ -z "${bottom_pane_id}" ]; then
        bottom_pane_id=$(wezterm cli split-pane)
    fi

    wezterm cli activate-pane-direction --pane-id "$bottom_pane_id" down

    send_to_bottom_pane="wezterm cli send-text --pane-id $bottom_pane_id --no-paste"
    program=$(wezterm cli list | awk -v pane_id="$bottom_pane_id" '$3==pane_id { print $6 }')
    if [ "$program" = "gitui" ]; then
        echo "q" | $send_to_bottom_pane
    fi
}

basedir=$(dirname "$filename")
basename=$(basename "$filename")
basename_without_extension="${basename%.*}"
extension="${filename##*.}"

case "$1" in
    "dired")
        split_pane_down
        echo "lr" | $send_to_bottom_pane
        ;;
    "gitui")
        split_pane_down
        program=$(wezterm cli list | awk -v pane_id="$bottom_pane_id" '$3==pane_id { print $6 }')
        if [ "$program" = "gitui" ]; then
            wezterm cli activate-pane-direction down
        else
            echo "gitui" | $send_to_bottom_pane
        fi
        ;;
    "compile")
        split_pane_down
        case "$extension" in
            "rs")
                if [ "$basedir" = "examples" ]; then
                    run_command="cargo check --example $basename_without_extension"
                elif [ "$basedir" = "src/bin" ]; then
                    run_command="cargo check --bin $basename_without_extension"
                else
                    run_command="cargo check"
                fi
                ;;
            "ex")
                run_command="mix compile"
                ;;
            "java")
                run_command="mvn compile"
                ;;
        esac
        echo "$run_command" | $send_to_bottom_pane
        ;;
    "test")
        split_pane_down
        case "$extension" in
            "rs")
                test_name=$(head -"$line_number" "$filename" | tail -1 | sed -n 's/^.*fn \([^ ]*\)().*$/\1/p')
                if [ "$basedir" = "examples" ]; then
                    run_command="cargo test --example $basename_without_extension $test_name"
                elif [ "$basedir" = "src/bin" ]; then
                    run_command="cargo test --bin $basename_without_extension $test_name"
                else
                    run_command="cargo test"
                fi
                ;;
            "exs")
                run_command="mix test $filename:$line_number"
                ;;
            "java")
                test_name=$(head -"$line_number" "$filename" | tail -1 | sed -n 's/^.*void \([^ ]*\)().*$/\1/p')
                run_command="mvn test -Dtest=$fileName#$test_name"
                ;;
        esac
        echo "$run_command" | $send_to_bottom_pane
        ;;
esac
