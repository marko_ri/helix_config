-- pull in the wezterm API
local wezterm = require 'wezterm'
-- this module helps us bind keys to actions
local act = wezterm.action

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

config.window_decorations = "RESIZE"
config.font = wezterm.font('Iosevka Fixed SS14')
config.font_size = 14
config.line_height = 1.2
config.color_scheme = 'Github (base16)'

config.disable_default_key_bindings = true
config.keys = {
   { key = 'o', mods = 'SHIFT|CTRL', action = wezterm.action.ShowDebugOverlay },
   { key = 'f', mods = 'SHIFT|CTRL', action = act.Search 'CurrentSelectionOrEmptyString' },

   { key = 'l', mods = 'SHIFT|CTRL', action = act.ActivatePaneDirection 'Right' },
   { key = 'h', mods = 'SHIFT|CTRL', action = act.ActivatePaneDirection 'Left' },
   { key = 'k', mods = 'SHIFT|CTRL', action = act.ActivatePaneDirection 'Up' },
   { key = 'j', mods = 'SHIFT|CTRL', action = act.ActivatePaneDirection 'Down' },
   { key = 'z', mods = 'SHIFT|CTRL', action = act.TogglePaneZoomState },
   { key = 'LeftArrow',  mods = 'SHIFT|CTRL', action = act.ActivateTabRelative(-1) },
   { key = 'RightArrow', mods = 'SHIFT|CTRL', action = act.ActivateTabRelative(1) },
   { key = 'UpArrow',    mods = 'SHIFT|CTRL', action = act.ScrollByPage(-1) },
   { key = 'DownArrow',  mods = 'SHIFT|CTRL', action = act.ScrollByPage(1) },

   { key = '+', mods = 'SHIFT|CTRL', action = act.IncreaseFontSize },
   { key = '-', mods = 'CTRL', action = act.DecreaseFontSize },
   { key = '0', mods = 'CTRL', action = act.ResetFontSize },

   { key = 'Enter', mods = 'SHIFT|CTRL', action = act.ActivateCopyMode },
   { key = 'c', mods = 'SHIFT|CTRL', action = act.CopyTo 'Clipboard' },
   { key = 'v', mods = 'SHIFT|CTRL', action = act.PasteFrom 'Clipboard' },

   { key = 'a', mods = 'SUPER', action = act.SplitVertical { domain = 'CurrentPaneDomain' } },
   { key = 's', mods = 'SUPER', action = act.SplitHorizontal { domain = 'CurrentPaneDomain' } },
   { key = 't', mods = 'SUPER', action = act.SpawnTab 'CurrentPaneDomain' },
}

config.hyperlink_rules = wezterm.default_hyperlink_rules()

-- make compilation errors into uri
table.insert(config.hyperlink_rules, {
  regex = '^( -->|\\*\\* \\(.*Error\\))? ([^:]+:\\d+):.+$',
  format = '$2',
  highlight = 2,
})

function editable(filename)
  -- "foo.bar" -> ".bar"
  local extension = filename:match("^.+(%..+)$")
  if extension then
    -- ".bar" -> "bar"
    extension = extension:sub(2)
    wezterm.log_info(string.format("extension is [%s]", extension))
    local binary_extensions = {
      jpg = true,
      png = true,
      pdf = true,
      mp4 = true,
      html = true,
    }
    if binary_extensions[extension] then
      -- can't edit binary files
      return false
    end
  end

  -- if there is no, or an unknown, extension, then assume
  -- that our trusty editor will do something reasonable

  return true
end

function extract_filename(uri)
  -- `file://hostname/path/to/file`
  local start, match_end = uri:find("file:");
  if start == 1 then
    -- skip "file://", -> `hostname/path/to/file`
    local host_and_path = uri:sub(match_end+3)
    local start, match_end = host_and_path:find("/")
    if start then
      -- -> `/path/to/file`
      return host_and_path:sub(match_end)
    end
  end

  return uri
end

wezterm.on('open-uri', function(window, pane, uri)
  local name = extract_filename(uri)
  if name and editable(name) then
    local direction = 'Up'
    local hx_pane = pane:tab():get_pane_direction(direction)
    if hx_pane == nil then
      local action = wezterm.action {
        SplitPane={
          direction = direction,
          command = { args = { os.getenv("EDITOR"), name } }
        };
      };
      window:perform_action(action, pane)
    else
      local action = wezterm.action.SendString(':open ' .. name .. '\r')
      window:perform_action(action, hx_pane)
    end
    window:perform_action(wezterm.action.ActivatePaneDirection 'Prev', hx_pane)

    -- prevent the default action from opening in a browser
    return false
  end
end)

-- and finally, return the configuration to wezterm
return config
